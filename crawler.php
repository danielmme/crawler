<?php

require 'vendor/autoload.php';


use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\VarDumper\VarDumper;
use League\Csv\Writer;


function cube($n, $m, $z) {
  $nodes = [$n, $m, $z];
return $nodes;
}
//13
$client = new Client( ['connect_timeout' => 60000]);

// for($n=21; $n < 100; $n++) {
  $n= 20;
  $url = "https://www.kickante.com.br/exportar-body?page={$n}";

  $resposta = $client->request('GET', $url);

  $html = $resposta->getBody();

  $crawler = new Crawler();
  $crawler->addHtmlContent($html);

  $count = count($crawler->filter('div.views-field-nid > span.field-content'));

  $nids = $crawler->filter('div.views-field-nid > span.field-content');
  $titles = $crawler->filter('div.views-field-title > span.field-content');

    
  $bodies = $crawler->filter('div.views-field-body > div.field-content');

  $html = [];  
  foreach ($bodies as $domElement) {
    $html[] = $domElement->ownerDocument->saveHTML($domElement);
  }

  $nid = [];
  foreach ($nids as $value) {
    $nid[] = $value->textContent;
  }

  $title = [];
  foreach ($titles as $value) {
    $title[] = $value->textContent;
  }

  $records = array_map("cube", $nid, $title, $html);
  $header = ['nid', 'title', 'body'];
  $csv = Writer::createFromString();

  //insert the header
  $csv->insertOne($header);
  //insert all the records
  $csv->insertAll($records);
  $p = 1 + $n;
  $file = "campanha_bodies_{1000}.csv";
  $data =  $csv->toString();

  file_put_contents($file, $data);

// }


// $fp = fopen('campanha_body.csv', 'w');

// foreach ($records as $node) {
//     fputcsv($fp, $node);
// }

// fclose($fp);


